const colors = require('tailwindcss/colors')

module.exports = {
    purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        fontFamily: {
            sans: ['"Poppins"', 'sans-serif'],
        },
        textColor: {
            'primary': '#A91F76',
            'white': '#ffffff',
            'danger': '#e3342f',
            pink: {
                DEFAULT: '#A91F76',
            },
        },
        colors: {
            'primary': '#310624',
            'secondary': '#3D062D',
            'white': '#ffffff',
            'red': '#c11313',
            'yellow': '#e5b91b',
            pink: {
                DEFAULT: '#A91F76',
            },
        },
        extend: {
            lineHeight: {
                "extra-thin": 0.9,
                "thin": 1
            },
            margin: {
                '72': '18rem',
                '76': '19rem',
                '80': '20rem',
                '100': '25rem',
            },
            maxWidth: {
                '1xl': '10rem',
                '2xl': '17rem',
                '3xl': '25rem',
                '4xl': '30rem',
                '80': '20rem'
            },
            maxHeight: {
                '0': '0',
                '1/4': '25%',
                '1/2': '50%',
                '3/4': '75%',
                'full': '100%',
            },
        },
    },
    variants: {
        extend: {
            width: ['responsive', 'hover'],
        },
    },
    plugins: [],
}
